<?php

namespace Danielozano\OrderFee\Model\Quote\Total;

use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Quote\Model\Quote\Address\Total\AbstractTotal;

class CustomFee extends AbstractTotal
{
    /**
     * @var PriceCurrencyInterface
     */
    private $_priceCurrency;

    /**
     * @param PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
        PriceCurrencyInterface $priceCurrency
    ) {
        $this->_priceCurrency = $priceCurrency;
    }

    /**
     * @param  \Magento\Quote\Model\Quote                          $quote
     * @param  \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param  \Magento\Quote\Model\Quote\Address\Total            $total
     * @return $this|bool
     */
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        parent::collect($quote, $shippingAssignment, $total);
        $customFee = $quote->getData('custom_fee');

        if ($customFee) {
            $customFeeAmount = $this->_priceCurrency->convert($customFee);
            $total->setTotalAmount('custom_fee', $customFee);
            $total->setBaseTotalAmount('custom_fee', $customFee);

            $total->setBaseGrandTotal($total->getBaseGrandTotal() + $customFee);
            $total->setGrandTotal($total->getGrandTotal() + $customFee);

            $total->setCustomFee($customFee);
            $quote->setCustomFee($customFee);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return __('Custom Free');
    }

    /**
     * @param  \Magento\Quote\Model\Quote               $quote
     * @param  \Magento\Quote\Model\Quote\Address\Total $total
     * @return array
     */
    public function fetch(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        return [
            'code' => 'custom_fee',
            'value' => $quote->getCustomFee(),
            'title' => __('Custom Fee')
        ];
    }
}
