<?php

namespace Danielozano\OrderFee\Model\Creditmemo\Total;

use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\Order\Creditmemo\Total\AbstractTotal;

class CustomFee extends AbstractTotal
{
    public function collect(Creditmemo $creditmemo)
    {
        /** @var Magento\Sales\Model\Order */
        $order = $creditmemo->getOrder();
        $customFeeAmount = $order->getCustomFee();

        if ($customFeeAmount) {
            $creditmemo->setData('custom_fee', $customFeeAmount);
            $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $customFeeAmount);
            $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $customFeeAmount);
        }

        return $this;
    }
}
