<?php

namespace Danielozano\OrderFee\Model\Invoice\Total;

use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Invoice\Total\AbstractTotal;

class CustomFee extends AbstractTotal
{
    /**
     * @param  Invoice $invoice
     * @return $this
     */
    public function collect(Invoice $invoice)
    {
        /** @var Magento\Sales\Model\Order */
        $order = $invoice->getOrder();

        $customFeeAmount = $order->getCustomFee();

        if ($customFeeAmount) {
            $invoice->setData('custom_fee', $customFeeAmount);
            $invoice->setGrandTotal($invoice->getGrandTotal() + $customFeeAmount);
            $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $customFeeAmount);
        }

        return $this;
    }
}
