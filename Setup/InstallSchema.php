<?php

namespace Danielozano\OrderFee\Setup;

use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\InstallSchemaInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param  SchemaSetupInterface   $setup
     * @param  ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->addCustomFeeColumn($setup);
        
        $setup->endSetup();
    }

    /**
     * Add custom_fee column to tables: quote, sales_order
     * sales_creditmemo, sales_invoice
     *
     * @param SchemaSetupInterface $setup
     * @return void
     */
    private function addCustomFeeColumn(SchemaSetupInterface $setup)
    {
        $orderRelatedTables =  [
            'quote',
            'sales_order',
            'sales_creditmemo',
            'sales_invoice'
        ];

        $customFeeColumnOptions = [
            'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,4',
            'default'   => 0.00,
            'nullable'  => true,
            'comment'   => 'custom_fee'
        ];

        foreach ($orderRelatedTables as $table) {
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($table),
                    'custom_fee',
                    $customFeeColumnOptions
                );
        }
    }
}
