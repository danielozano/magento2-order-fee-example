<?php

namespace Danielozano\OrderFee\Block\Adminhtml\Order\Totals;

class CustomFee extends \Magento\Sales\Block\Adminhtml\Totals
{
    /**
     * Add custom fee to order totals
     * @return $this
     */
    public function initTotals()
    {
        if (!$this->getSource()->getCustomFee()) {
            return $this;
        }

        $customFee = $this->getSource()->getCustomFee();

        $customFeeTotal = new \Magento\Framework\DataObject(
            [
                'code' => 'custom_fee',
                'value' => $customFee,
                'base_value' => $customFee,
                'label' => __('Custom Fee')
            ]
        );

        $this->getParentBlock()->addTotal($customFeeTotal, 'subtotal');

        return $this;
    }
}
