<?php

namespace Danielozano\OrderFee\Block\Adminhtml\Creditmemo\Totals;

use Magento\Sales\Block\Order\Totals;

class CustomFee extends Totals
{
    /**
     * Add custom fee to creditmemo totals summary.
     *
     * @return $this
     */
    public function initTotals()
    {
        $customFee = $this->getSource()->getCustomFee();

        if ($customFee) {
            $customFeeTotal = new \Magento\Framework\DataObject([
                'code'        => 'custom_fee',
                'value'       => $customFee,
                'base_valuue' => $customFee,
                'label'       => __('Custom Fee')
            ]);

            $this->getParentBlock()->addTotal($customFeeTotal, 'adjustments');
        }

        return $this;
    }
}
