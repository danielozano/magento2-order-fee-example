<?php

namespace Danielozano\OrderFee\Block\Adminhtml\Invoice\Totals;

use Magento\Sales\Block\Order\Totals;

class CustomFee extends Totals
{

    /**
     * @return $this
     */
    public function initTotals()
    {
        if ($this->getSource()->getCustomFee()) {
            $customFee = $this->getSource()->getCustomFee();

            $customFeeTotal = new \Magento\Framework\DataObject([
                'code'       => 'custom_fee',
                'value'      => $customFee,
                'base_value' => $customFee,
                'label'      => __('Custom Fee')
            ]);

            $this->getParentBlock()->addTotal($customFeeTotal, 'shipping');
        }

        return $this;
    }
}
