define(
    [
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils',
        'Magento_Checkout/js/model/totals'
    ],
    function(AbstractTotal, Quote, PriceUtils, Totals) {
        "use strict";

        return AbstractTotal.extend({
            defaults: {
                title: Totals.getSegment('custom_fee').title
            },
            
            totals: Quote.getTotals(),

            isDisplayed: function() {
                return this.getValue() > 0;
            },
            
            getValue: function() {
                var price = 0;

                if (this.totals() && Totals.getSegment('custom_fee')) {
                    price = Totals.getSegment('custom_fee').value;
                }

                return price;
            }
        });
    }
);