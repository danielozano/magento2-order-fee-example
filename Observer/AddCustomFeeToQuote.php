<?php

namespace Danielozano\OrderFee\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class AddCustomFeeToQuote implements ObserverInterface
{
    const CUSTOM_FEE_CONFIG_PATH = 'custom_fee/general/amount';

    /**
     * @var ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * Add custom fee to quote
     * @param  Observer $event
     * @return void
     */
    public function execute(Observer $event)
    {
        /** @var Magento\Quote\Model\Quote */
        $quote = $event->getQuote();

        /** @var string|null */
        $customFeeAmount = $this->_scopeConfig->getValue(self::CUSTOM_FEE_CONFIG_PATH);

        $quote->setData('custom_fee', $customFeeAmount);
    }
}
