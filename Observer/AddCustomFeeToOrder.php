<?php

namespace Danielozano\OrderFee\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class AddCustomFeeToOrder implements ObserverInterface
{
    /**
     * Add custom_fee to order
     * @param  Observer $observer
     * @return $this
     */
    public function execute(Observer $observer)
    {
        /** @var Magento\Quote\Model\Quote */
        $quote = $observer->getQuote();
        /** @var Magento\Sales\Model\Order */
        $order = $observer->getOrder();

        /** @var float|null */
        $customFee = $quote->getCustomFee();

        if ($customFee) {
            $order->setData('custom_fee', $customFee);
        }

        return $this;
    }
}
