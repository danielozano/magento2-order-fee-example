# MAGENTO 2 ORDER FEE

Ejemplo que muestra como añadir un impuesto extra al flujo completo de pedido.

## DONE
 - Importe de impuesto configurable.
 - Creación de campos nuevos en tablas relacionadas con el pedido.
 - Cálculo de impuesto en los totales del quote.
 - Cálculo de impuesto en los totales de invoice.
 - Cálculo de impuesto en los totales de creditmemo.
 - Vista de impuesto en el totals summary del carrito.
 - Vista de impuesto en el totals summary del checkout.
 - Vista de impuesto en el totals summary de la vista de pedido admin.
 - Vista de impuesto en el totals summary de la vista/edición de creditmemo admin.
 - Vista de impuesto en el totals summary de la vista/edición de invoice admin.
 - Vista de línea de impuesto en invoice PDF.
 - Vista de línea de impuesto en creditmemo PDF.
 - Vista de línea de impuesto en vista de pedido/invoice/creditmemo guest.
 - Añadir línea de impuesto a order/invoice/creditmemo PDF vista guest.
 - Añadir línea de impuesto a email de order/invoice/creditmemo.
## BACKLOG
 - Añadir línea de impuesto a vista order/invoice/creditmemo cliente.
 - Añadir línea de impuesto a order/invoice/creditmemo PDF vista cliente.